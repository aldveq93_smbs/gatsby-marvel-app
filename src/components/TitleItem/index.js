import { Link } from 'gatsby';
import React from 'react';

const TitleItem = ({urlDestination, itemTitle, resourceUri}) => {

    const getMarvelResourceId = marvelResourceStr => {
        const resourceId = marvelResourceStr.match(/\d+/g) ;
        if (resourceId) {
            return resourceId[1];
        }
    }

    const marvelResourceId = getMarvelResourceId(resourceUri);
    return (
        <li><Link to={`/${urlDestination}/${marvelResourceId}`}>{itemTitle}</Link></li>
    );
}

export default TitleItem;