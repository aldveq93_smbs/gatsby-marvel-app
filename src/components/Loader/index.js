import React from 'react';

const Loader = () => {
    return(
        <section className="loader-section">
            <div className="lds-ripple"><div></div><div></div></div>
        </section>
    );
}

export default Loader;