import { Link } from 'gatsby';
import React, { useCallback, useEffect, useState } from 'react';
import { getMarvelTitleOrName, getMarvelImage } from '../../utilities/utilities';

const MarvelCard = ( { urlDetail, buttonDetail, marvelData, handlerDataChange = null } ) => { 
    const [isHeartIconActive, setIsHeartIconActive] = useState(false);
    const marvelDataName = getMarvelTitleOrName(marvelData);
    const marvelDataDescription = (marvelData?.description !== '' && marvelData?.description !== null) ? marvelData?.description : 'Description not available :('; 
    const marvelDataDescriptionTextLength = marvelDataDescription.length > 50 && 'hide-text';
    const marvelDataImage = getMarvelImage(marvelData);

    const matchDataInLS = useCallback(() => {
        const marvelDataFromLS = JSON.parse(localStorage.getItem('marvelData'));
        if(marvelDataFromLS !== undefined && marvelDataFromLS !== null && marvelDataFromLS.length > 0) {
            const matchedMarvelData = marvelDataFromLS.find(mvData => mvData.data.id === marvelData.id)
            if(matchedMarvelData !== undefined && matchedMarvelData !== null) {
                setIsHeartIconActive(true);
            } else {
                setIsHeartIconActive(false);
            }
        }
    }, [setIsHeartIconActive, marvelData]);

    useEffect(() => {
        matchDataInLS();
    }, [matchDataInLS]);

    const saveMarvelData = () => {
        const marvelDataType = getMarvelDataType();
        const formattedMarvelData = {
            type: marvelDataType,
            data: marvelData
        };

        if(isMarvelDataAvailableInStorage()) {
            updateMarvelDataInStorage(formattedMarvelData);
        } else {
            createMarvelDataInStorage(formattedMarvelData);
        }

    }

    const createMarvelDataInStorage = formattedMarvelData => {
        const newMarvelDataArray = [];
        newMarvelDataArray.push(formattedMarvelData);
        localStorage.setItem('marvelData', JSON.stringify(newMarvelDataArray));
        setIsHeartIconActive(true);

        if (handlerDataChange !== null) {
            handlerDataChange(JSON.parse(localStorage.getItem('marvelData')));
        } 
    }

    const updateMarvelDataInStorage = formattedMarvelData => {
        const { id } = formattedMarvelData.data;
        const marvelDataFromLS = JSON.parse(localStorage.getItem('marvelData'));
        const marvelDataFound = marvelDataFromLS.find(mvData => mvData.data.id === id);

        if (marvelDataFound !== undefined) { // Remove it
            const updatedMarvelData = marvelDataFromLS.filter(mvDataLS => mvDataLS.data.id !== marvelDataFound.data.id );
            localStorage.setItem('marvelData', JSON.stringify(updatedMarvelData));
            setIsHeartIconActive(false);

            if (handlerDataChange !== null) {
                handlerDataChange(JSON.parse(localStorage.getItem('marvelData')));
            }
        } else { // Add it
            marvelDataFromLS.push(formattedMarvelData);
            localStorage.setItem('marvelData', JSON.stringify(marvelDataFromLS));
            setIsHeartIconActive(true);
            
            if (handlerDataChange !== null) {
                handlerDataChange(JSON.parse(localStorage.getItem('marvelData')));
            }
        }
    }

    const isMarvelDataAvailableInStorage = () => {
        const marvelDataInStorage = localStorage.getItem('marvelData');

        if (marvelDataInStorage !== undefined && marvelDataInStorage !== null && marvelDataInStorage.length > 0)
            return true;

        return false;
    }

    const getMarvelDataType = () => {
        if('comics' in marvelData && 'stories' in marvelData)
            return 'character';
        
        if('characters' in marvelData && 'stories' in marvelData)
            return 'comic';

        if('characters' in marvelData && 'comics' in marvelData)
            return 'story';
    }

    return(
        <div className="marvel-card-inner animate__animated animate__bounceIn animate__delay-2s">
            <article className="marvel-card-inner__card-container">
                <img src={`${marvelDataImage}`} alt={marvelDataName} className="marvel-card-inner__thumbnail" />
                <div className="marvel-card-inner__info-container">
                    <div className="marvel-card-inner__heart-name-container">
                        <span className="marvel-card-inner__name">{marvelDataName}</span>
                        <span className={`marvel-card-inner__heart ${isHeartIconActive && 'active'}`} onClick={saveMarvelData} onKeyDown={saveMarvelData} role="button" tabIndex={0} aria-label="Save Marvel Data"></span>
                    </div>
                    <div className="marvel-card-inner__flexible-container">
                        <span className={`marvel-card-inner__desc ${marvelDataDescriptionTextLength}`}>{marvelDataDescription}</span>
                    </div>
                    <Link to={`/${urlDetail}/${marvelData?.id}`} className="marvel-card-inner__details-button">{buttonDetail}</Link>
                </div>
            </article>        
        </div>
    );
}

export default MarvelCard;