import { Link } from 'gatsby';
import React, { useState } from 'react';

const Navigation = () => {

    const [isActive, setIsActive] = useState(false);

    const handlerHamburger = () => {
        isActive ? setIsActive(false) : setIsActive(true);
    }

    const onNavItemClick = (value) => () => {
        setIsActive(value);
    }

    return(
        <>
            <button className={`hamburger hamburger--collapse ${isActive && 'is-active'}`} type="button" onClick={ handlerHamburger }>
                <span className="hamburger-box" onClick={ handlerHamburger } onKeyDown={ handlerHamburger } role="button" aria-label="Open Navigation" tabIndex={0}>
                    <span className="hamburger-inner" onClick={ handlerHamburger } onKeyDown={ handlerHamburger } role="button" aria-label="Open Navigation" tabIndex={0}></span>
                </span>
            </button>
            <nav className={`main-navigation ${isActive && 'show'}`}>
                <ul className="main-navigation__wrapper">
                    <li className="main-navigation__item">
                        <div onClick={onNavItemClick(false)} onKeyDown={onNavItemClick(false)} role="button" aria-label="Close Navigation" tabIndex={0}>
                            <Link to="/">Home</Link>
                        </div>
                    </li>
                    <li className="main-navigation__item">
                        <div onClick={onNavItemClick(false)} onKeyDown={onNavItemClick(false)} role="button" aria-label="Close Navigation" tabIndex={0}>
                            <Link to="/characters">Characters</Link>
                        </div>
                    </li>
                    <li className="main-navigation__item">
                        <div onClick={onNavItemClick(false)} onKeyDown={onNavItemClick(false)} role="button" aria-label="Close Navigation" tabIndex={0}>
                            <Link to="/comics">Comics</Link>
                        </div>
                    </li>
                    <li className="main-navigation__item">
                        <div onClick={onNavItemClick(false)} onKeyDown={onNavItemClick(false)} role="button" aria-label="Close Navigation" tabIndex={0}>
                            <Link to="/stories">Stories</Link>
                        </div>
                    </li>
                    <li className="main-navigation__item">
                        <div onClick={onNavItemClick(false)} onKeyDown={onNavItemClick(false)} role="button" aria-label="Close Navigation" tabIndex={0}>
                            <Link to="/favorites">Favorites</Link>
                        </div>
                    </li>
                </ul>
                <img src='../marvel.svg' className="main-navigation__logo" alt="Marvel App" />
            </nav>
        </>
    );
}

export default Navigation;