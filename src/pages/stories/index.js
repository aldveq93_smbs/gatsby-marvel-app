import React, { useCallback, useEffect, useState } from 'react'
import { Feedback, Loader, MainTitle } from '../../components';
import { ThirdService } from '../../utilities/third-service';
import MarvelDataContainer from '../../containers/MarvelDataContainer';
import MainLayout from '../../containers/MainLayout';

const Stories = () => {

    const [stories, setStories] = useState({
        total: 0,
        filter: '',
        keyword: '',
        data: []
    });
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);
    
    const getMarvelStories = useCallback(async () => {
        const marvelService = new ThirdService();
        setLoading(true);
        const marvelStories = await marvelService.getStories();
        setStories({
            total: marvelStories.total,
            filter: 'allStories',
            keyword: '',
            data: marvelStories.data
        });
        setLoading(false);
    }, [setStories, setLoading]);

    const drawStories = () => {
        if (loading)
            return <Loader />

        if (error) 
            return <Feedback feedbackType="error" message="Sorry! We didn't find available data!" getMarvelData={getMarvelStories} setError={setError} />
 
        return(
            <MarvelDataContainer urlDetail='stories' buttonDetail='See story' marvelDataToRender={stories} />
        );
    };

    useEffect(() => {
        getMarvelStories();
    }, [getMarvelStories]);

    return (
        <MainLayout>
            <MainTitle title="Marvel Stories" />
            { drawStories() }
        </MainLayout>
    )
}

export default Stories;