import React, { useState, useEffect } from 'react'
import { FeedbackRedirection, Loader, MainTitle } from '../../components';
import MarvelDataContainer from '../../containers/MarvelDataContainer';
import MainLayout from '../../containers/MainLayout';

const Favorites = () => {

    const [marvelDataFromLS, setMarvelDataFromLS] = useState([]);
    const [laoding, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true);
        setMarvelDataFromLS(JSON.parse(localStorage.getItem('marvelData')));
        setLoading(false);
    }, []);

    const isMarvelDataAvailable = marvelDataArray => {
        if(marvelDataArray !== undefined && marvelDataArray !== null && marvelDataArray.length > 0)
            return true;
        
        return false;
    }

    const drawFavourites = () => {
        
        if(marvelDataFromLS === null) 
            return <FeedbackRedirection feedbackType='error' message='Oh, you have not saved favorites items!' urlRedirection='/' />

        const marvelCharactersData = marvelDataFromLS.filter(mvData => mvData.type === 'character');
        const formatMarvelCharactersData = getFormattedData(marvelCharactersData);

        const marvelComicsData = marvelDataFromLS.filter(mvData => mvData.type === 'comic');
        const formatMarvelComicsData = getFormattedData(marvelComicsData);

        const marvelStoriesData = marvelDataFromLS.filter(mvData => mvData.type === 'story');
        const formatMarvelStoriesData = getFormattedData(marvelStoriesData);

        if(laoding)
            return <Loader/>

        if(!isMarvelDataAvailable(marvelCharactersData) && !isMarvelDataAvailable(marvelComicsData) && !isMarvelDataAvailable(marvelStoriesData))
            return <FeedbackRedirection feedbackType='error' message='Oh, you have not saved favorites items!' urlRedirection='/' />
        

        if(!isMarvelDataAvailable(marvelComicsData) && !isMarvelDataAvailable(marvelStoriesData)) { // Render Characters   
            return(
                <MainLayout>
                    <MainTitle title='Your favorites items' />
                    { drawMarvelData('Characters', 'characters', 'See character', formatMarvelCharactersData, setMarvelDataFromLS) }
                </MainLayout>
            )
        }

        if(!isMarvelDataAvailable(marvelCharactersData) && !isMarvelDataAvailable(marvelStoriesData)) { // Render Comics
            return(
                <MainLayout>
                    <MainTitle title='Your favorites items' />
                    { drawMarvelData('Comics', 'comics', 'See comic', formatMarvelComicsData, setMarvelDataFromLS) }

                </MainLayout>
            )
        }

        if(!isMarvelDataAvailable(marvelCharactersData) && !isMarvelDataAvailable(marvelComicsData)) { // Render Stories
            return(
                <MainLayout>
                    <MainTitle title='Your favorites items' />
                    { drawMarvelData('Stories', 'stories', 'See story', formatMarvelStoriesData, setMarvelDataFromLS) }

                </MainLayout>
            )
        }

        if(!isMarvelDataAvailable(marvelCharactersData)) { // Render Comics & Stories
            return(
                <MainLayout>
                    <MainTitle title='Your favorites items' />
                    { drawMarvelData('Comics', 'comics', 'See comic', formatMarvelComicsData, setMarvelDataFromLS) }
                    { drawMarvelData('Stories', 'stories', 'See story', formatMarvelStoriesData, setMarvelDataFromLS) }
                </MainLayout>
            )
        }

        if(!isMarvelDataAvailable(marvelComicsData)) { // Render Characters & Stories
            return(
                <MainLayout>
                    <MainTitle title='Your favorites items' />
                    { drawMarvelData('Characters', 'characters', 'See character', formatMarvelCharactersData, setMarvelDataFromLS) }
                    { drawMarvelData('Stories', 'stories', 'See story', formatMarvelStoriesData, setMarvelDataFromLS) }
                </MainLayout>
            )
        }

        if(!isMarvelDataAvailable(marvelStoriesData)) { // Render Characters & Comics
            return(
                <MainLayout>
                    <MainTitle title='Your favorites items' />
                    { drawMarvelData('Characters', 'characters', 'See character', formatMarvelCharactersData, setMarvelDataFromLS) }
                    { drawMarvelData('Comics', 'comics', 'See comic', formatMarvelComicsData, setMarvelDataFromLS) }
                </MainLayout>
            )
        }

        if(isMarvelDataAvailable(marvelCharactersData) && isMarvelDataAvailable(marvelComicsData) && isMarvelDataAvailable(marvelStoriesData)) { // Render Characters & Comcis & Stories
            return(
                <MainLayout>
                    <MainTitle title='Your favorites items' />
                    { drawMarvelData('Characters', 'characters', 'See character', formatMarvelCharactersData, setMarvelDataFromLS) }
                    { drawMarvelData('Comics', 'comics', 'See comic', formatMarvelComicsData, setMarvelDataFromLS) }
                    { drawMarvelData('Stories', 'stories', 'See story', formatMarvelStoriesData, setMarvelDataFromLS) }
                </MainLayout>
            )
        }

        return (
            <FeedbackRedirection feedbackType='error' message='Oh, you have not saved favorites items!' urlRedirection='home' />
        )
    }

    const getFormattedData = data => {
        const objArray =  data.map(dt => dt.data);
        return {
            data: objArray
        }
    }

    const drawMarvelData = (dataTitle, urlDetail, buttonDetail, dataToRender, handlerChange) => {
        return(
            <>
                <MainTitle title={dataTitle} />
                <MarvelDataContainer urlDetail={urlDetail} buttonDetail={buttonDetail} marvelDataToRender={dataToRender} handlerChange={handlerChange} />
            </>
        );
    }

    return (
        drawFavourites()
    )
}

export default Favorites;