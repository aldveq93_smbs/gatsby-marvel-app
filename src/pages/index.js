import React from "react"
import MainLayout from "../containers/MainLayout";

export default function Home() {
  return(
    <MainLayout>
        <div className="home-section animate__animated animate__fadeIn">
            <img src='/marvel.svg' className="home-section__logo" alt="Marvel App" />
            <p>
                Welcome to the Marvel App!
            </p>
        </div>
    </MainLayout>
  );
}
