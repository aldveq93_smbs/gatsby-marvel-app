import React from 'react'
import MainLayout from '../containers/MainLayout';
import { Link } from 'gatsby';

const Page404 = () => {
    return (
        <MainLayout>
            <div className="not-fount-page-container">
                <h1>404</h1>
                <p>We are sorry, we didn't find the resource you were looking for!</p>
                <Link to='/'>Go home</Link>
            </div>
        </MainLayout>
    )
}

export default Page404;