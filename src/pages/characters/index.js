import React, { useCallback, useEffect, useState } from 'react';
import { MainTitle, Loader, Filter, FeedbackRedirection, Feedback } from '../../components';
import MarvelDataContainer from '../../containers/MarvelDataContainer';
import MainLayout from '../../containers/MainLayout';
import { ThirdService } from '../../utilities/third-service';

const Characters = () => {

    const [characters, setCharacters] = useState({
        total: 0,
        filter: '',
        keyword: '',
        data: []
    });
    const [loading, setLoading] = useState(false);
    const [errorFeedbackRedirection, setErrorFeedbackRedirection] = useState({active: false, message: '', destination:''});
    const [errorFeedbackResetData, setErrorFeedbackResetData] = useState({active: false, message: ''});
    
    const getMarvelCharacthers = useCallback(async () => {
        const marvelService = new ThirdService();
        setLoading(true);
        const marvelCharacters = await marvelService.getCharacters();

        if(marvelCharacters.error) {
            const { message } = marvelCharacters.error;
            setLoading(false);
            setErrorFeedbackRedirection({active: true, message: `Oh, there was an error on your marvel characters request: ${message}!`, destination:'/'});
            return;
        }

        setCharacters({
            total: marvelCharacters.total,
            filter: 'allCharacters',
            keyword: '',
            data: marvelCharacters.data
        });
        setLoading(false);
    }, [setCharacters, setLoading]);

    const drawCharacters = () => {
        if (errorFeedbackRedirection.active) 
            return <FeedbackRedirection feedbackType='error' message={errorFeedbackRedirection.message} urlRedirection={errorFeedbackRedirection.destination} />

        if (errorFeedbackResetData.active)
            return <Feedback feedbackType='error' message={errorFeedbackResetData.message} getMarvelData={getMarvelCharacthers} setError={setErrorFeedbackResetData} />
  
        return(
            <>
                <div style={{ display: loading ? 'block' : 'none' }}>
                    <Loader />
                </div>
                <div style={{ display: loading ? 'none' : 'block' }}>
                    <Filter updateMarvelData={setCharacters} resetMarvelData={getMarvelCharacthers} setLoading={setLoading} setError={setErrorFeedbackResetData} marvelFilterType='characters'/>
                    <MarvelDataContainer urlDetail='characters' buttonDetail='See character' marvelDataToRender={characters} />
                </div>
            </>
        );
    };

    useEffect(() => {        
        getMarvelCharacthers();
    }, [getMarvelCharacthers]);

    return(
        <MainLayout>
            <MainTitle title='Marvel Characters' />
            { drawCharacters() }
        </MainLayout>
    );
}

export default Characters;