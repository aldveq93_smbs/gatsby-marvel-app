import React, { useCallback, useEffect, useState } from 'react';
import { Loader, MarvelGallery, MarvelItemDetails } from '../../components';
import MainLayout from '../../containers/MainLayout';
import { ThirdService } from '../../utilities/third-service';

const SingleComic = ({ id }) => {
    const [loading, setLoading] = useState(false);
    const [comicData, setComicData] = useState({});
    const [comicCharacters, setComicCharacters] = useState([]);
    const [comicStories, setComicStories] = useState([]);
    const [comicGallery, setComicGallery] = useState([]);

    const getComicDataById = useCallback(async () => {
        const marvelService = new ThirdService();
        setLoading(true);
        const comicDataFromMarvel = await marvelService.getComicById(id);
        setComicData(comicDataFromMarvel);
        setComicCharacters(comicDataFromMarvel.characters.items);
        setComicStories(comicDataFromMarvel.stories.items);
        formatGallery(comicDataFromMarvel.images);
        setLoading(false);
    },[id, setLoading]);

    useEffect(() => {
        getComicDataById();
    }, [getComicDataById]);

    const formatGallery = comicImages => {
        const formatComicImages = comicImages.map(cI => {
            return {
                url: `${cI.path}.${cI.extension}`,
                thumbUrl: `${cI.path}.${cI.extension}`
            }
        })
        setComicGallery(formatComicImages);
    }

    const drawComic = () => {
        if(loading)
            return <Loader/>

        return (
            <MainLayout>
                <MarvelItemDetails marvelDataDetails={comicData} listOne={comicCharacters} listTwo={comicStories} titleListOne='Characters' titleListTwo='Stories' urlListOne='characters' urlListTwo='stories' />
                { comicGallery.length > 0 &&  <MarvelGallery marvelGalleryName='Comic Gallery' marvelGalleryData={comicGallery} /> }
            </MainLayout> 
        );
    }

    return (
       drawComic()    
    );
}

export default SingleComic;