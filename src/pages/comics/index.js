import React, { useCallback, useEffect, useState } from "react";
import { Feedback, FeedbackRedirection, Filter, Loader, MainTitle } from "../../components";
import MarvelDataContainer from '../../containers/MarvelDataContainer';
import MainLayout from '../../containers/MainLayout';
import { ThirdService } from "../../utilities/third-service";

const Comics = () => {

    const [comics, setComics] = useState({
        total: 0,
        filter: '',
        keyword: '',
        data: []
    });
    const [loading, setLoading] = useState(false);
    const [errorFeedbackRedirection, setErrorFeedbackRedirection] = useState({active: false, message: '', destination:''});
    const [errorFeedbackResetData, setErrorFeedbackResetData] = useState({active: false, message: ''});
    
    const getMarvelComics = useCallback( async ()=> {
        const marvelService = new ThirdService();
        setLoading(true);
        const marvelComics = await marvelService.getComics();

        if(marvelComics.error) {
            const { message } = marvelComics.error;
            setLoading(false);
            setErrorFeedbackRedirection({active: true, message: `Oh, there was an error on your marvel comics request: ${message}!`, destination:'/'});
            return;
        }

        setComics({
            total: marvelComics.total,
            filter: 'allComics',
            keyword: '',
            data: marvelComics.data
        });
        setLoading(false);   
    }, [setLoading, setComics]);

    const drawComics = () => {
        if(errorFeedbackRedirection.active) 
            return <FeedbackRedirection feedbackType="error" message={`${errorFeedbackRedirection.message}`} urlRedirection={errorFeedbackRedirection.destination} />

        if (errorFeedbackResetData.active)
            return <Feedback feedbackType='error' message={errorFeedbackResetData.message} getMarvelData={getMarvelComics} setError={setErrorFeedbackResetData} />

        return(
            <>
                <div style={{display: loading ? 'block' : 'none' }}>
                    <Loader/>
                </div>
                <div style={{display: loading ? 'none' : 'block' }}>
                    <Filter updateMarvelData={setComics} resetMarvelData={getMarvelComics} setLoading={setLoading} setError={setErrorFeedbackResetData} marvelFilterType='comics'/>
                    <MarvelDataContainer urlDetail='comics' buttonDetail='See comic' marvelDataToRender={comics} />
                </div>
            </>
        );

    };

    useEffect(() => {
        getMarvelComics();
    }, [getMarvelComics]);

    return(
        <MainLayout>
            <MainTitle title='Marvel Comics' />
            { drawComics() }
        </MainLayout>
    );
}

export default Comics;