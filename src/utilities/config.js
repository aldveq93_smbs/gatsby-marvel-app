const marvelPublicKey = process.env.GATSBY_MARVEL_PUBLIC_KEY;
const marvelPrivateKey = process.env.GATSBY_MARVEL_PRIVATE_KEY;
const marvelRequestDataLimit = process.env.GATSBY_MARVEL_REQUEST_DATA_LIMIT; 

export {
    marvelPublicKey,
    marvelPrivateKey,
    marvelRequestDataLimit
}