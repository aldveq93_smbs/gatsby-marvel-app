import React from 'react';
import { Navigation } from '../../components';
import '../../../node_modules/animate.css';
import '../../assets/scss/style.scss';

const MainLayout = ({ children }) => {
    return (
        <section className="App">
            <Navigation/>
            <section className="marvel-main-container">
                {children}  
            </section>
        </section>
    )
}

export default MainLayout;